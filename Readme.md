![Preview Image](https://vtolvr-mods.com/media/uploaded_mod_images/web_previewnNUGOqnYJr.png)
# [Download](https://vtolvr-mods.com/mod/t276rqpk/)
# Adds a switch in the cockpit to turn on a smoke trail behind you.

You can set the colour in the mod settings page where you load the mod and also change the lifetime of the smoke (default 5)

# Known bugs
- The switch still says APU under it
- Some particals don't get shifted when the world shifts

# FAQ
## Does it work with Multiplayer
No not really, only the client with see their own smoke.
## Can you bind it to the trigger?
I may do this in the future.