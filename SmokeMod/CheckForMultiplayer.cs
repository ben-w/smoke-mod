﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace SmokeMod
{
    static class CheckForMultiplayer
    {
        public static bool Check()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            Assembly[] assems = currentDomain.GetAssemblies();

            for (int i = 0; i < assems.Length; i++)
            {
                if (assems[i].FullName.Contains("Multiplayer"))
                    return true;
            }
            return false;
        }
    }
}
