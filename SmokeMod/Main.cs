using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using UnityEngine;

namespace SmokeMod
{
    public class Main : VTOLMOD
    {
        private static int _r = 255;
        private static int _g = 255;
        private static int _b = 255;
        private static float _lifeTime = 5f;

        private const string _assetBundleFile = "assets";
        private static GameObject _prefab;
        private static string _path;
        private ParticleSystem _currentSmoke;
        public override void ModLoaded()
        {
            if (CheckForMultiplayer.Check())
                EnableLogger();

            CreateSettings();
            _path = Path.Combine(ThisMod.ModFolder, _assetBundleFile);
            StartCoroutine(LoadAssetBundle());
            VTOLAPI.SceneLoaded += SceneLoaded;
            VTOLAPI.MissionReloaded += MissionReloaded;
        }
        private IEnumerator LoadAssetBundle()
        {
            Log($"This is my mods folder {ThisMod.ModFolder}");
            if (!File.Exists(_path))
            {
                LogError("Asset Bundle is missing from the path\nPath: " + _path);
                yield break;
            }
            AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(_path);
            yield return request;
            AssetBundleRequest smokeRequest = request.assetBundle.LoadAssetAsync("smoke", typeof(GameObject));
            yield return smokeRequest;
            if (smokeRequest.asset == null)
            {
                LogError("Smoke seems to be missing inside of the asset bundle");
                yield break;
            }
            _prefab = smokeRequest.asset as GameObject;
            Log("Loaded!");
        }
        private ParticleSystem CreateSmoke()
        {
            Log("Creating Smoke");
            return Instantiate(_prefab)
                .AddComponent<FOParticleShifter>()
                .GetComponent<ParticleSystem>();
        }
        private void SceneLoaded(VTOLScenes scene)
        {
            switch (scene)
            {
                case VTOLScenes.OpenWater:
                case VTOLScenes.Akutan:
                case VTOLScenes.CustomMapBase:
                case VTOLScenes.CustomMapBase_OverCloud:
                    AttachSmoke();
                    break;
            }
        }
        private void MissionReloaded()
        {
            AttachSmoke();
        }
        private void AttachSmoke()
        {
            _currentSmoke = CreateSmoke();
            _currentSmoke.Stop();

            GameObject player = VTOLAPI.GetPlayersVehicleGameObject();
            _currentSmoke.transform.parent = player.transform;

            switch (VTOLAPI.GetPlayersVehicleEnum())
            {
                case VTOLVehicles.None:
                    LogError("The API returned none");
                    return;
                case VTOLVehicles.AV42C:
                    CreateButton(new Vector3(47.6f, 80, -0.1f));
                    _currentSmoke.transform.localPosition = new Vector3(0, -0.514f, -7.02f);
                    _currentSmoke.transform.localRotation = Quaternion.Euler(180, 0, 0);
                    break;
                case VTOLVehicles.FA26B:
                    CreateButton(new Vector3(145.7f, 207.2f, -8.4f));
                    _currentSmoke.transform.localPosition = new Vector3(0, -0.575f, -4.795f);
                    _currentSmoke.transform.localRotation = Quaternion.Euler(180, 0, 0);
                    break;
                case VTOLVehicles.F45A:
                    CreateButton(new Vector3(85.7f, -77.4f, -0.1f));
                    _currentSmoke.transform.localPosition = new Vector3(0, 0, -4.6f);
                    _currentSmoke.transform.localRotation = Quaternion.Euler(180, 0, 0);
                    break;
            }

            SetSmokeColour();
            SetSmokeLifeTime();
        }
        private void CreateButton(Vector3 position)
        {
            Log("Creating Button");
            VTText label = new VTText();
            label.text = "Smoke";
            label.fontSize = 31.2f;
            label.lineHeight = 1;
            label.align = VTText.AlignmentModes.Center;
            label.vertAlign = VTText.VerticalAlignmentModes.Middle;
            label.color = Color.white;
            label.emission = Color.green;

            VRLever lever = null;
            GameObject newSwitch = null;
            SwitchAPI.CopyAPUSwitch("Smoke", ref lever, ref newSwitch, localPosition: position, label: label);

            lever.OnSetState.AddListener(ToggleSmoke);
        }
        public void ToggleSmoke(int value)
        {
            if (value == 1)
            {
                _currentSmoke.Play();
            }
            else
            {
                _currentSmoke.Pause();
            }
        }
        private void SetSmokeColour()
        {
            ParticleSystem.ColorOverLifetimeModule a = _currentSmoke.colorOverLifetime;
            Gradient grad = new Gradient();
            grad.SetKeys(
                new GradientColorKey[]
                {
                    new GradientColorKey(new Color(_r, _g, _b),0f),
                    new GradientColorKey(new Color(_r, _g, _b), 1f)
                },
                new GradientAlphaKey[]{
                    new GradientAlphaKey(1f,0f),
                    new GradientAlphaKey(1f, 0.9f),
                    new GradientAlphaKey(0f, 0f)
                });

            a.color = grad;
        }
        private void SetSmokeLifeTime()
        {
            ParticleSystem.MainModule main = _currentSmoke.main;
            main.startLifetime = _lifeTime;
        }

        private void CreateSettings()
        {
            Settings settings = new Settings(this);
            settings.CreateCustomLabel("Set Smoke Colour");
            settings.CreateIntSetting("Red", RedSet, 255, 0, 255);
            settings.CreateIntSetting("Green", GreenSet, 255, 0, 255);
            settings.CreateIntSetting("Blue", BlueSet, 255, 0, 255);
            settings.CreateFloatSetting("Smoke Lifetime (seconds)", SetSmokeLifeTime, 5f, 0);
            VTOLAPI.CreateSettingsMenu(settings);
        }
        public void RedSet(int value)
        {
            _r = value;
            Log($"Changed colour to {_r},{_g},{_b}");
        }
        public void GreenSet(int value)
        {
            _g = value;
            Log($"Changed colour to {_r},{_g},{_b}");
        }
        public void BlueSet(int value)
        {
            _b = value;
            Log($"Changed colour to {_r},{_g},{_b}");
        }
        public void SetSmokeLifeTime(float value)
        {
            _lifeTime = value;
            Log($"Changed smoke lifetime to {value}");
        }
        public static void EnableLogger()
        {
            // In case the user has loaded the multiplayer mod
            Debug.unityLogger.logEnabled = true;
        }
    }
}