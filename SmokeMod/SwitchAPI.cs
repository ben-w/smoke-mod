﻿/* This script is a basic API for adding custom buttons
 * It's heavily based off of C-137's API, just tweaked to
 * make it easier for me to use.
 * 
 * I have also shortened it as I didn't need all of it.
 * 
 * https://github.com/LSantos2003/Custom-Buttons-and-Switches/blob/API_TEST/NewPlane/CustomAPI.cs
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using Harmony;

public static class SwitchAPI
{
    private enum Switch { APU }
    private static GameObject FindOriginalSwitches(Switch switchToFind)
    {
        switch (switchToFind)
        {
            case Switch.APU:
                return GameObject.Find("APUSwitch");
        }
        return null;
    }
    private static AudioClip FindSwitchAudio()
    {
        AudioClip[] clips = Resources.FindObjectsOfTypeAll<AudioClip>();
        for (int i = 0; i < clips.Length; i++)
        {
            if (clips[i].name == "switch")
                return clips[i];
        }
        return null;
    }
    private static void Log(object message)
    {
        // I have done this so that I have the API instead
        // of the mod's name
        Debug.Log("[Switch API] " + message);
    }
    private static void LogWarning(object message)
    {
        // I have done this so that I have the API instead
        // of the mod's name
        Debug.LogWarning("[Switch API] " + message);
    }
    private static void LogError(object message)
    {
        // I have done this so that I have the API instead
        // of the mod's name
        Debug.LogError("[Switch API] " + message);
    }

    /// <summary>
    /// Creates a APU style switch. (Switch with the red cover)
    /// </summary>
    /// <param name="switchName">The name of your switch</param>
    /// <param name="bound"></param>
    /// <param name="boundName"></param>
    /// <returns></returns>
    public static void CopyAPUSwitch(string switchName, ref VRLever lever, ref GameObject newSwitch, PoseBounds bound = null, string boundName = "", Vector3 localPosition = default, VTText onText = null, VTText offText = null, VTText label = null)
    {
        GameObject orignalSwitch = FindOriginalSwitches(Switch.APU);

        if (orignalSwitch == null)
        {
            LogError("Couldn't find the APU Switch");
            return;
        }

        if (boundName == "")
        {
            boundName = switchName + " Bounds";
        }

        newSwitch = GameObject.Instantiate(orignalSwitch);
        newSwitch.transform.SetParent(orignalSwitch.transform.parent, false);
        newSwitch.name = switchName;
        newSwitch.transform.localPosition = localPosition;

        VRInteractable switchInteractable = newSwitch.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<VRInteractable>();

        VRInteractable coverInteractable = null;
        if (VTOLAPI.GetPlayersVehicleEnum() != VTOLVehicles.AV42C)
        {
            coverInteractable = newSwitch.transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<VRInteractable>();
            if (VTOLAPI.GetPlayersVehicleEnum() == VTOLVehicles.F45A)
            {
                Log("This is the F45A");
                GameObject.Destroy(newSwitch.transform.GetChild(5).gameObject);
            }
            else
            {
                Log("This is the FA-26B");
            }
        }
        else
        {
            Log("This is the AV42C");
            coverInteractable = newSwitch.transform.GetChild(4).GetChild(0).GetChild(0).GetChild(0).GetComponent<VRInteractable>();
        }


        if (switchInteractable == null)
        {
            LogError("Switch Interactable is null. " + newSwitch.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).name);
        }
        else
        {
            switchInteractable.interactableName = switchName;
        }

        if (coverInteractable == null)
        {
            LogError("Switch Interactable is null. " + newSwitch.transform.GetChild(4).GetChild(0).GetChild(0).name);
        }
        else
        {
            coverInteractable.interactableName = switchName + " cover";
        }



        //Setting PoseBounds
        if (bound == null)
        {
            Log("Bounds is null, so creating a new one");
            // This adds PoseBounds Component to a child of the switch 
            // This is so that when you move it around, you move both posebounds
            // and your switch around
            if (newSwitch == null)
            {
                LogError("newSwitch == null");
            }
            else if (newSwitch.transform.GetChild(0) == null)
            {
                LogError("newSwitch's child is null");
            }
            Log("New switch is fine");
            GameObject newBounds = newSwitch.transform.GetChild(0).gameObject;
            if (newBounds == null)
            {
                LogError("newBounds == null");
            }
            else
            {
                Log("newBounds is fine");
            }
            PoseBounds bounds = newBounds.AddComponent<PoseBounds>();
            if (bounds == null)
            {
                LogError("bounds == null");
            }



            Log("Setting bounds size");
            // The scale of the child is over 1300, so that's why these are so small
            bounds.size = new Vector3(0.2f, 0.2f, 0.2f);
            Log("Created bounds");
            if (switchInteractable == null)
            {
                LogError("Switch Interactable is null. ");
            }
            if (coverInteractable == null)
            {
                LogError("Switch Interactable is null. ");
            }
            switchInteractable.poseBounds = bounds; //Assigns bounds for switch
            coverInteractable.poseBounds = bounds; //Assigns bounds for cover

        }
        else
        {
            Log("Using existing bounds");
            switchInteractable.poseBounds = bound; //Assigns bounds for cover switch
            coverInteractable.poseBounds = bound; //Assigns bounds for cover
        }

        Log("Getting VR Lever");
        lever = switchInteractable.GetComponent<VRLever>();
        if (lever == null)
        {
            LogError("Lever is null " + switchInteractable.name);
        }
        // When you try removing a listener it seemed not to work in 
        // past experiences, so I just re find the audio again
        lever.OnSetState = new IntEvent();

        GameObject audioGO = GameObject.Find("RightFwdDashAudio");
        if (audioGO != null)
        {
            AudioSource audio = audioGO.GetComponent<AudioSource>();
            AudioClip switchAudio = FindSwitchAudio();
            if (switchAudio != null)
            {
                Log("Setting back switch audio");
                lever.OnSetState.AddListener(delegate { audio.PlayOneShot(switchAudio); });
            }
            else
            {
                LogWarning("The switch audio couldn't be found");
            }
        }
        else
        {
            LogWarning("Couldn't find RightFwdDashAudio, new button won't have any sound");
        }

        // This is destroying the text below the switch as I couldn't 
        // managed to get it updating.
        int offset = 0;
        if (VTOLAPI.GetPlayersVehicleEnum() != VTOLVehicles.AV42C)
        {
            offset = 1;
        }
        GameObject.Destroy(newSwitch.transform.GetChild(3 + offset));
    }
}